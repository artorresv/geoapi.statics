**REQUERIMIENTOS**

  * Ubuntu > 18.x
  * Nodejs > 8.x
  * npm > 6.x (creo que se instala por omisión con nodejs)

**INSTALACION**

0. Crear directorios para archivos temporales y para subida de datos:

    `sudo mkdir -p /datos/geoapi/upload/`
    
    `sudo mkdir -p /datos/geoapi/temp/`
    
    `sudo chown -R <user>:<group> /datos`
    
    `chmod -R g+w /datos`
    
    `# Donde:`
    
    `#   <user>: el usuario que ejecutará los servicios`
    
    `#   <group>: el grupo correspondiente`

1. Asumiendo que este directorio existe y se tienen permisos de escritura:

    `cd /opt/webserver`

2. Clonar el repositorio:

    `git clone https://artorresv@bitbucket.org/artorresv/geoapi.statics.git`
    
3. El procedimiento anterior crea un directorio con el código fuente:

    `cd geoapi.statics`

4. Instalar las dependencias de nodejs:

    `NODE_ENV=production npm install`

5. Después de instaladas, copiar los siguientes archivos (revisar el contenido para ver qué puerto usa, etc.) según la instalación sea para desarrollo o para producción:

    Producción: `scp geos1.conabio.gob.mx:/home/atorres/bin/geoportal/servicios/geoapi.statics/production.config.json .`
    
    Desarrollo: `scp geos1.conabio.gob.mx:/home/atorres/bin/geoportal/servicios/geoapi.statics/development.config.json .`

**USO**

1. Incio normal de la aplicación:
    
    a. Con npm:
    
       `NODE_ENV=production npm run start`
       
    b. Con pm2 (ver ecosystem.config.json para más detalles):
    
       `pm2 start ecosystem.config.json --env production`
      
2. Incio para [depuración](https://nodejs.org/en/docs/guides/debugging-getting-started/) con npm:

    `NODE_ENV=production npm run start:debug`
    
3. Operaciones:
      
      1. La única operación disponible es la publicación de archivos estáticos con express:
      
        http://geoportal.conabio.gob.mx/descargas/mapas/dinamicos/
         
4. Comentarios:
     * En este directorio se guardan archivos estáticos de otros dos servicios, geoapi.downloads y geoapi
     
**ADVERTENCIAS**

* Ninguna

**PENDIENTES**

* CRON Job para eliminar archivos caducados
